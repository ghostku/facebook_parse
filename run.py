# FB
import robobrowser
import re
import json
import csv
import sys
import os
from lib.gfunc.progressbar import progressbar



class Facebook(robobrowser.RoboBrowser):

    url = 'https://facebook.com'

    def __init__(self, email, password):
        self.email = email
        self.password = password
        super().__init__(parser='lxml')
        self.login()

    def login(self):
        self.open(self.url)
        login_form = self.get_form(id='login_form')
        login_form['email'] = self.email
        login_form['pass'] = self.password
        self.submit_form(login_form)
        # open('text.html', 'w', encoding="utf-8").write(str(self.parsed))

    def get_page_fans(self, page_id):
        start = 0
        all_users = set()
        while True:
            self.open('https://www.facebook.com/browse/?type=page_fans&page_id={id}&start={start}'.format(id=page_id, start=start * 100))
            users = self.select('a._ohe')
            if not users:  # Выходим если нет пользователей
                break
            all_users.update([i['href'] for i in users])
            start += 1
        return all_users

    def _get_by_id_from_comments(self, id):
        from bs4 import BeautifulSoup, Comment
        node = self.find(text=lambda text: isinstance(text, Comment) and id in text.string)
        soap = BeautifulSoup(node.string, 'lxml')
        return soap.find(id=id)

    def _get_by_class_from_comments(self, class_):
        from bs4 import BeautifulSoup, Comment
        node = self.find(text=lambda text: isinstance(text, Comment) and class_ in text.string)
        soap = BeautifulSoup(node.string, 'lxml')
        return soap.find(class_=class_)

    def get_user_info(self, user_url):
        # Номер / Имя / Город / Должность / Компания / Количество друзей / Ссылка на профиль /
        self.open(user_url)
        open('user.html', 'w', encoding="utf-8").write(str(self.parsed))
        try:
            all_data = self._get_by_id_from_comments('intro_container_id')
        except AttributeError:
            all_data = None
        user = {'city': 'n/a',
                'job_title': 'n/a',
                'company': 'n/a'
                }
        user['name'] = self._get_by_id_from_comments('fb-timeline-cover-name').text
        user['url'] = user_url
        if all_data:
            city = all_data.find(class_=S['cl_CITY'])
            if city:
                user['city'] = city.parent.find('a').text
            job = all_data.find(class_=S['cl_WORK'])
            if job:
                user['job_title'] = job.parent.find('a').parent.text
                user['company'] = job.parent.find('a').text
        try:
            user['friends'] = self._get_by_class_from_comments(S['cl_FRIENDS']).parent.parent.find(lambda i: re.match('^\d+$', i.text)).text
        except AttributeError:
            user['friends'] = 'n/a'
        return user

S = json.load(open("default.conf", encoding='utf-8'), encoding='utf-8')
S.update(json.load(open("work.conf", encoding='utf-8'), encoding='utf-8'))

if not os.path.exists(S['DATA_FILE']):
    with open(S['DATA_FILE'], 'w', encoding='utf-8-sig') as file:
        print(';'.join(S['HEADER']), file=file)
# exit(1)
# TODO: Вынести чтение и запись csv в отдельную функцию
with open(S['DATA_FILE'], 'r', encoding='utf-8') as file:
    file.read(1)  # Пропускаем строку с UTF-8 BOM
    fp = csv.DictReader(file, dialect='excel', delimiter=';')
    data = [row for row in fp]
    headers = fp.fieldnames if fp.fieldnames else S['HEADER']
# print(headers)
# exit(1)
urls = [i['url'] for i in data]
print('В нашей базе уже содержится {qnty} аккаунтов.'.format(qnty=len(urls)))

fb = Facebook(S['MAIN_USER']['LOG'], S['MAIN_USER']['PWD'])
fb.login
ids = fb.get_page_fans(S['GROUP_ID'])
print('Нашу страницу лайкнули {qnty} аккаунтов.'.format(qnty=len(ids)))
ids = list(filter(lambda x: x not in urls, ids))
print('Из них новых для нас {qnty}.'.format(qnty=len(ids)))

fb = Facebook(S['TEST_USER']['LOG'], S['TEST_USER']['PWD'])
fb.login

# TODO: DONE Ввод с командной строки параметра ограничивающего число элементов в переборе
try:
    MAX_ITEMS = min(int(sys.argv[1]), len(ids))
except (IndexError, ValueError):
    MAX_ITEMS = len(ids)

print('Переберем {qnty} аккаунтов.'.format(qnty=MAX_ITEMS))
users = []
pb = progressbar(MAX_ITEMS)
pb.start()
for id in list(ids)[:MAX_ITEMS]:
    users.append(fb.get_user_info(id))  # Получаем данные о человеке
    pb.count()
pb.stop()
with open(S['DATA_FILE'], "a", encoding="utf-8", newline='\n') as file:
    fp = csv.DictWriter(file, headers, dialect='excel', delimiter=';')
    fp.writerows(users)

del fb
print('Готово')
exit(0)
